#include "RenderingGame.h"
#include "GameException.h"
#include "FirstPersonCamera.h"
#include "TriangleDemo.h"

#include "Keyboard.h"
#include "Mouse.h"
#include "ModelFromFile.h"
#include "FpsComponent.h"
#include "RenderStateHelper.h"
#include "ObjectDiffuseLight.h"
#include "ObjectSpecularLight.h"
#include "SamplerStates.h"
#include "RasterizerStates.h"
#include <SpriteFont.h>
#include <sstream>
#include <windows.h>
#include <thread>
#include <chrono>



namespace Rendering
{
	;



	const XMFLOAT4 RenderingGame::BackgroundColor = { 0.75f, 0.75f, 0.75f, 1.0f };

	RenderingGame::RenderingGame(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand)
		: Game(instance, windowClass, windowTitle, showCommand),
		mDemo(nullptr), mDirectInput(nullptr), mKeyboard(nullptr), mMouse(nullptr), mModel1(nullptr),
		mFpsComponent(nullptr), mRenderStateHelper(nullptr), mObjectDiffuseLight(nullptr), treeDiffuseLight(nullptr), mModel2(nullptr), mSpriteFont(nullptr), mSpriteBatch(nullptr),
		woodDiffuseLight(nullptr), benchDiffuseLight(nullptr), benchSpecularLight(nullptr), houseBuilding(nullptr), doorModelSpecular(nullptr), deskModel(nullptr),
		greyModel(nullptr), blackModel(nullptr), brownModel(nullptr)

	{
		mDepthStencilBufferEnabled = true;
		mMultiSamplingEnabled = true;
	}

	RenderingGame::~RenderingGame()
	{
	}

	void RenderingGame::Initialize()
	{

		mCamera = new FirstPersonCamera(*this);
		mComponents.push_back(mCamera);
		mServices.AddService(Camera::TypeIdClass(), mCamera);

		mDemo = new TriangleDemo(*this, *mCamera);
		mComponents.push_back(mDemo);

		//Remember that the component is a management class for all objects in the D3D rendering engine. 
		//It provides a centralized place to create and release objects. 
		//NB: In C++ and other similar languages, to instantiate a class is to create an object.
		if (FAILED(DirectInput8Create(mInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID*)&mDirectInput, nullptr)))
		{
			throw GameException("DirectInput8Create() failed");
		}

		mKeyboard = new Keyboard(*this, mDirectInput);
		mComponents.push_back(mKeyboard);
		mServices.AddService(Keyboard::TypeIdClass(), mKeyboard);

		mMouse = new Mouse(*this, mDirectInput);
		mComponents.push_back(mMouse);
		mServices.AddService(Mouse::TypeIdClass(), mMouse);

		mModel2 = new ModelFromFile(*this, *mCamera, "Content\\Models\\charModelMix.dae", L"Tester", 20, 0,"human1");
		mModel2->SetPosition(-1.57f, -0.0f, -0.0f, 0.15f, -5.0f, 1.0f, -17.0f);
		mComponents.push_back(mModel2);

#pragma region 
		// Any positional changes on treeDiffuseLight MUST be added to woodDiffuseLight also
		// First Tree (Right)


		for (int i = 0; i < 4; i++)
		{
			float x = 8 + i * 4;
			for (int j = 0; j < 2; j++)
			{
				float y = -20 - j * 4;
				treeDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 1);
				treeDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, x, 0.0f, y);
				mComponents.push_back(treeDiffuseLight);

				woodDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 3);
				woodDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, x, 0.0f, y);
				mComponents.push_back(woodDiffuseLight);
			}
		}

		for (int i = 0; i < 4; i++)
		{
			float x = -24 + i * 4;
			for (int j = 0; j < 2; j++)
			{
				float y = -20 - j * 4;
				treeDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 1);
				treeDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, x, 0.0f, y);
				mComponents.push_back(treeDiffuseLight);

				woodDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 3);
				woodDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, x, 0.0f, y);
				mComponents.push_back(woodDiffuseLight);
			}
		}


		//// Second Tree (Left)
		//treeDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 1);
		//treeDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, 12.0f, 0.0f, -20.5f);
		//mComponents.push_back(treeDiffuseLight);

		//woodDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 3);
		//woodDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, 12.0f, 0.0f, -20.5f);
		//mComponents.push_back(woodDiffuseLight);
		//#pragma endregion Palm Tree (Leaves/Wood)

		//// Third Tree
		//treeDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 1);
		//treeDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, 16.0f, 0.0f, -20.5f);
		//mComponents.push_back(treeDiffuseLight);

		//woodDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 3);
		//woodDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 1.0f, 16.0f, 0.0f, -20.5f);
		//mComponents.push_back(woodDiffuseLight);

#pragma region 
// Bench Diffuse
		benchDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 4);
		benchDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 0.011f, -7.5f, 0.90f, -17.0f);
		mComponents.push_back(benchDiffuseLight);
		benchDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 4);
		benchDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 0.011f, -2.5f, 0.90f, -17.0f);
		mComponents.push_back(benchDiffuseLight);
#pragma endregion New Bench

#pragma region
		// Character Diffuse
		characterDiffuseLight = new ObjectDiffuseLight(*this, *mCamera, 2);
		characterDiffuseLight->SetPosition(-1.57f, -0.0f, -0.0f, 0.15f, 0.0f, 1.0f, -17.0f);
		mComponents.push_back(characterDiffuseLight);
#pragma endregion Character(s)

#pragma region
		houseBuilding = new ObjectSpecularLight(*this, *mCamera, 5);
		houseBuilding->SetPosition(-1.57f, -0.0f, -0.0f, 0.7f, 0.0f, 0.00f, -20.5f);
		// scale
		mComponents.push_back(houseBuilding);

		
		doorModelSpecular = new ObjectDiffuseLight(*this, *mCamera, 6);
		doorModelSpecular->SetPosition(-1.57f, -0.0f, -0.0f, 0.7f, 0.0f, 0.00f, -20.5f);
		mComponents.push_back(doorModelSpecular);


		// Desk
		blackModel = new ObjectSpecularLight(*this, *mCamera, 9);
		blackModel->SetPosition(-1.57f, -0.0f, -0.0f, 0.25f, -2.0f, 0.30f, -25.5f);
		mComponents.push_back(blackModel);

		greyModel = new ObjectSpecularLight(*this, *mCamera, 8);
		greyModel->SetPosition(-1.57f, -0.0f, -0.0f, 0.25f, -2.0f, 0.30f, -25.5f);
		mComponents.push_back(greyModel);

		brownModel = new ObjectSpecularLight(*this, *mCamera, 10);

		brownModel->SetPosition(-1.57f, -0.0f, -0.0f, 0.25f, -2.0f, 0.30f, -25.5f);
		mComponents.push_back(brownModel);


		// Trash
		trashCanModel = new ObjectSpecularLight(*this, *mCamera, 11);
		trashCanModel->SetPosition(-1.57f, -0.0f, -0.0f, 0.25f, -4.0f, 0.03f, -25.5f);
		mComponents.push_back(trashCanModel);

		// Key
		keyModel = new ModelFromFile(*this, *mCamera, "Content\\Models\\keyModel.3ds", L"", 5, 5,"key");
		keyModel->SetPosition(0.0f, -0.0f, -0.0f, 0.30f, -15.0f, 1.0f, -20.5f);
		mComponents.push_back(keyModel);



		// Computer
		computerBase = new ObjectDiffuseLight(*this, *mCamera, 7);
		computerBase->SetPosition(-1.57f, -0.0f, -0.0f, 0.25f, -2.5f, 1.26f, -25.9f);
		mComponents.push_back(computerBase);

		offScreen = new ModelFromFile(*this, *mCamera, "Content\\Models\\computerScreen.3ds", L"", 10, 4,"offScreen");
		offScreen->SetPosition(-1.57f, 0.0f, 0.0f, 0.25f, -2.5f, 1.26f, -25.9f);
		mComponents.push_back(offScreen);

		computerScreen = new ModelFromFile(*this, *mCamera, "Content\\Models\\computerScreen.3ds", L"Screen", 10, 3,"Computer");
		computerScreen->SetPosition(-1.57f, 0.0f, 0.0f, 0.25f, -2.5f, 1.26f, -25.9f);
		mComponents.push_back(computerScreen);

#pragma endregion All House Objects


		seatTop = new ObjectDiffuseLight(*this, *mCamera, 8);
		seatTop->SetPosition(-1.57f, 1.5f, -0.0f, 0.35f, -2.5f, 0.6f, -25.0f);
		mComponents.push_back(seatTop);

		seatBottom = new ObjectSpecularLight(*this, *mCamera, 16);
		seatBottom->SetPosition(-1.57f, 1.5f, -0.0f, 0.35f, -2.5f, 0.6f, -25.0f);
		mComponents.push_back(seatBottom);



		#pragma region 
		carWindows = new ObjectSpecularLight(*this, *mCamera, 17);
		carWindows->SetPosition(-1.57f, 1.5f, 1.6f, 1.0f, 14.0f, 1.0f, 60.0f);
		mComponents.push_back(carWindows);

		carBase = new ObjectSpecularLight(*this, *mCamera, 18);
		carBase->SetPosition(-1.57f, 1.5f, 1.6f, 1.0f, 14.0f, 1.0f, 60.0f);
		mComponents.push_back(carBase);

		carWheels = new ObjectSpecularLight(*this, *mCamera, 19);
		carWheels->SetPosition(-1.57f, 1.5f, 1.6f, 1.0, 14.0f, 1.0f, 60.0f);
		mComponents.push_back(carWheels);
		#pragma endregion Car


		roadFloor = new ObjectSpecularLight(*this, *mCamera, 20);
		roadFloor->SetPosition(-1.57f, 1.5f, -1.5f, 0.35f, 13.5f, 0.0f, -45.0f);
		mComponents.push_back(roadFloor);


		chickenModel = new ModelFromFile(*this, *mCamera,"Content\\Models\\chickenModel.3ds", L"",10, 6, "chicken");
		chickenModel->SetPosition(-1.57f, 1.5f, -0.0f, 0.35f, 4.5f, 0.6f, -25.0f);
		mComponents.push_back(chickenModel);


		mFpsComponent = new FpsComponent(*this);
		mFpsComponent->Initialize();
		mRenderStateHelper = new RenderStateHelper(*this);

		mSpriteBatch = new SpriteBatch(mDirect3DDeviceContext);
		mSpriteFont = new SpriteFont(mDirect3DDevice, L"Content\\Fonts\\Arial_14_Regular.spritefont");

		RasterizerStates::Initialize(mDirect3DDevice);
		SamplerStates::Initialize(mDirect3DDevice);

		Game::Initialize();

		mCamera->SetPosition(0.0f, 1.0f, 10.0f);

	}

	void RenderingGame::Shutdown()
	{
		DeleteObject(mDemo);
		DeleteObject(mCamera);

		DeleteObject(mSpriteFont);
		DeleteObject(mSpriteBatch);

		DeleteObject(mKeyboard);
		DeleteObject(mMouse);
		ReleaseObject(mDirectInput);

		DeleteObject(mModel1);
		DeleteObject(mModel2);
		DeleteObject(mObjectDiffuseLight);
		DeleteObject(deskModel);
		DeleteObject(treeDiffuseLight);
		DeleteObject(houseBuilding);

		DeleteObject(computerScreen);
		DeleteObject(computerBase);

		DeleteObject(greyModel);
		DeleteObject(blackModel);
		DeleteObject(brownModel);

		DeleteObject(woodDiffuseLight);
		DeleteObject(benchDiffuseLight);
		DeleteObject(characterDiffuseLight);
		DeleteObject(benchSpecularLight);

		DeleteObject(mFpsComponent);
		DeleteObject(mRenderStateHelper);


		Game::Shutdown();
	}

	void RenderingGame::Update(const GameTime &gameTime)
	{
		
		mFpsComponent->Update(gameTime);
		Game::Update(gameTime);

		
		//Add "ESC" to exit the application
		if (mKeyboard->WasKeyPressedThisFrame(DIK_ESCAPE))
		{
			Exit();
		}

		//bounding box , we need to see if we need to do the picking test
		if (Game::toPick)
		{
			if (keyModel->Visible()) {
				Pick(Game::screenX, Game::screenY, keyModel);
				Game::toPick = false;
			}
			if (offScreen->Visible()) {
				Pick(Game::screenX, Game::screenY, offScreen);
				Game::toPick = false;
			}
			if (mModel2->Visible())
			{
				Pick(Game::screenX, Game::screenY, mModel2);
			}
			if (chickenModel->Visible())
				Pick(Game::screenX, Game::screenY, chickenModel);
		}
	}

	bool computerTrigger = true;
	bool keyTrigger = true;
	bool dialog = true;
	// do the picking here

	void RenderingGame::Pick(int sx, int sy, ModelFromFile* model)
	{
		//XMMATRIX P = mCam.Proj(); 
		XMFLOAT4X4 P;
		XMStoreFloat4x4(&P, mCamera->ProjectionMatrix());


		//Compute picking ray in view space.
		float vx = (+2.0f*sx / Game::DefaultScreenWidth - 1.0f) / P(0, 0);
		float vy = (-2.0f*sy / Game::DefaultScreenHeight + 1.0f) / P(1, 1);

		// Ray definition in view space.
		XMVECTOR rayOrigin = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
		XMVECTOR rayDir = XMVectorSet(vx, vy, -1.0f, 0.0f);

		// Tranform ray to local space of Mesh via the inverse of both of view and world transform

		XMMATRIX V = mCamera->ViewMatrix();
		XMMATRIX invView = XMMatrixInverse(&XMMatrixDeterminant(V), V);


		XMMATRIX W = XMLoadFloat4x4(model->WorldMatrix());
		XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(W), W);

		XMMATRIX toLocal = XMMatrixMultiply(invView, invWorld);

		rayOrigin = XMVector3TransformCoord(rayOrigin, toLocal);
		rayDir = XMVector3TransformNormal(rayDir, toLocal);

		// Make the ray direction unit length for the intersection tests.
		rayDir = XMVector3Normalize(rayDir);


		float tmin = 0.0;



		// We do stuff here when we 'Pick' the door
		if (model->mBoundingBox.Intersects(rayOrigin, rayDir, tmin) && (dialog == true) && model->getObjectTag() == "human1")
		{
			std::wostringstream pickupString;
			if(keyTrigger == true)
				pickupString << L"Can you help me get into my house, i've misplaced my key, help me find it please.";
			else
				pickupString << L"Thank you for helping get into my house, you're a hero. Can you check on my chicken please.";

			int result = MessageBox(0, pickupString.str().c_str(), L"Object Found", MB_ICONASTERISK | MB_YESNO);

			if (result == IDYES)
			{

			}			
		}	
		if (model->mBoundingBox.Intersects(rayOrigin, rayDir, tmin) &&  model->getObjectTag() == "chicken")
		{
			std::wostringstream pickupString;			
			pickupString << L"Bkawwwww";
			

			int result = MessageBox(0, pickupString.str().c_str(), L"Object Found", MB_ICONASTERISK | MB_YESNO);

			if (result == IDYES)
			{

			}
		}
		
		if (model->mBoundingBox.Intersects(rayOrigin, rayDir, tmin) && (computerTrigger == true) && model->getObjectTag()=="offScreen")
		{
			std::wostringstream pickupString;
			pickupString << L"You turned on the computer." << (model->GetModelDes()).c_str() << '\n' << '\t' << '+' << model->ModelValue() << L" points";

			int result = MessageBox(0, pickupString.str().c_str(), L"Object Found", MB_ICONASTERISK | MB_YESNO);

			if (result == IDYES)
			{

				//hide the object
				model->SetVisible(false);

				//update the score
				mScore += model->ModelValue();
				computerTrigger = false;
			}
		}
		if (model->mBoundingBox.Intersects(rayOrigin, rayDir, tmin) && (keyTrigger == true) && model->getObjectTag() == "key")
		{
			std::wostringstream pickupString;
			pickupString << L"You found a key!" << (model->GetModelDes()).c_str() << '\n' << '\t' << '+' << model->ModelValue() << L" points";

			int result = MessageBox(0, pickupString.str().c_str(), L"Object Found", MB_ICONASTERISK | MB_YESNO);

			if (result == IDYES)
			{
				model->SetVisible(false);	// hide key
				doorModelSpecular->SetPosition(-1.57f, -30.0f, -0.0f, 0.7f, -1.2f, 0.00f, -16.4f);  // open door

																									//update the score
				mScore += model->ModelValue();
				keyTrigger = false;
			}
		}

	
	
}
    void RenderingGame::Draw(const GameTime &gameTime)
    {
        mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&BackgroundColor));
        mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

        Game::Draw(gameTime);
		mRenderStateHelper->SaveAll();
		mFpsComponent->Draw(gameTime);
		
		mSpriteBatch->Begin();
		//draw the score
		std::wostringstream scoreLabel;
		scoreLabel << L"Total Points: " << mScore << "\n";
		mSpriteFont->DrawString(mSpriteBatch, scoreLabel.str().c_str(), XMFLOAT2(0.0f, 120.0f), Colors::Red);
		mSpriteBatch->End();

		mRenderStateHelper->RestoreAll();
       
        HRESULT hr = mSwapChain->Present(0, 0);
        if (FAILED(hr))
        {
            throw GameException("IDXGISwapChain::Present() failed.", hr);
        }

    }
}