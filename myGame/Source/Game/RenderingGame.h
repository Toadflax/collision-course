#pragma once


#include "Game.h"

using namespace Library;

namespace Library
{
    class FirstPersonCamera;
	class RenderStateHelper;

	class Keyboard;
	class Mouse;
	class FpsComponent;

}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}


namespace Rendering
{
    class TriangleDemo;
	class ModelFromFile;
	class ObjectDiffuseLight;
	class ObjectSpecularLight;

    class RenderingGame : public Game
    {
    public:
        RenderingGame(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand);
        ~RenderingGame();

        virtual void Initialize() override;		
        virtual void Update(const GameTime& gameTime) override;
        virtual void Draw(const GameTime& gameTime) override;


	protected:
        virtual void Shutdown() override;

    private:
		static const XMFLOAT4 BackgroundColor;
        FirstPersonCamera * mCamera;
        TriangleDemo* mDemo;

		//Define member variables for Keyboard and mouse
		LPDIRECTINPUT8 mDirectInput;

		Keyboard* mKeyboard;
		Mouse*    mMouse;
		ModelFromFile* mModel1;
		ModelFromFile* mModel2;

		int mScore;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;

		FpsComponent* mFpsComponent;
		RenderStateHelper* mRenderStateHelper;

		ObjectDiffuseLight* mObjectDiffuseLight;

		// tree obj
		ObjectDiffuseLight* treeDiffuseLight;
		ObjectDiffuseLight* woodDiffuseLight;
		ObjectDiffuseLight* benchDiffuseLight;
		ObjectDiffuseLight* characterDiffuseLight;
		ObjectSpecularLight* houseBuilding;

		//ObjectSpecularLight* doorModelSpecular;
		ObjectDiffuseLight* doorModelSpecular;
		ModelFromFile* offScreen;

		ObjectSpecularLight* deskModel;

		ObjectSpecularLight* brownModel;
		ObjectSpecularLight* greyModel;
		ObjectSpecularLight* blackModel;

		ObjectSpecularLight* trashCanModel;

		ObjectDiffuseLight* computerBase;
		ModelFromFile* computerScreen;

		ModelFromFile* keyModel;

		ObjectDiffuseLight* seatTop;
		ObjectSpecularLight* seatBottom;

		ObjectSpecularLight* carWindows;
		ObjectSpecularLight* carBase;
		ObjectSpecularLight* carWheels;

		ObjectSpecularLight* roadFloor;

		ModelFromFile* chickenModel;



		ObjectSpecularLight* benchSpecularLight;

		void Pick(int sx, int sy, ModelFromFile*);

	

    };
}